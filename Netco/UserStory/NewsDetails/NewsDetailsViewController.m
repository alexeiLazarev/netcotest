//
//  NewsDetailsViewController.m
//  Netco
//
//  Created by Alexei Lazarev on 10/22/20.
//

#import "NewsDetailsViewController.h"
#import <SDWebImage/SDWebImage.h>
#import "Masonry.h"

#define HEIGHT_RATIO 9
#define WIDTH_RATIO 16
#define OFFSET 10

@interface NewsDetailsViewController ()

@property (strong, nonatomic) UIScrollView* scrollView;
@property (strong, nonatomic) UIView* superview;
@property (strong, nonatomic) UIView* contentView;
@property (strong, nonatomic) UIImageView* imageView;
@property (strong, nonatomic) UILabel* titleView;
@property (strong, nonatomic) UILabel* textView;

@end

@implementation NewsDetailsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = UIColor.whiteColor;

    [self setupView];
}

- (void)setupView {
    _superview = self.view;
    [self setupScrollView];
    [self setupContentView];
    [self setupImageView];
    [self setupTitleView];
    [self setupTextView];
}

- (void)setupTextView {
    self.textView = [UILabel new];

    NSAttributedString *attrStr = [[NSAttributedString alloc] initWithData:[self.model.body dataUsingEncoding:NSUTF8StringEncoding]
                                                                options:@{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType,
                                                                          NSCharacterEncodingDocumentAttribute:@(NSUTF8StringEncoding)}
                                                     documentAttributes:nil
                                                                  error:nil];

    self.textView.attributedText = attrStr;
    [self.textView setFont:[UIFont systemFontOfSize:14 weight:UIFontWeightSemibold]];
    self.textView.numberOfLines = 0;

    [self.contentView addSubview: self.textView];

    [self.textView  mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_titleView.mas_bottom).with.offset(OFFSET);
        make.width.equalTo(@(_superview.bounds.size.width - 2 * OFFSET));
        make.left.equalTo(self.contentView).with.offset(OFFSET);
        make.right.equalTo(self.contentView).with.offset(-OFFSET);
        make.bottom.equalTo(self.contentView).with.offset(-OFFSET);
    }];
}

- (void)setupTitleView {
    self.titleView = [UILabel new];
    self.titleView.text = self.model.headline;
    self.titleView.numberOfLines = 2;

    [self.contentView addSubview: self.titleView];

    [self.titleView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.imageView.mas_bottom).with.offset(OFFSET);
        make.width.equalTo(@(self.superview.bounds.size.width - 2 * OFFSET));
        make.left.equalTo(self.contentView).with.offset(OFFSET);
        make.right.equalTo(self.contentView).with.offset(-OFFSET);
    }];
}

- (void)setupContentView {
    self.contentView = [UIView new];

    [self.scrollView addSubview:self.contentView];

    [self.contentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.scrollView.mas_top);
        make.left.equalTo(self.scrollView.mas_left);
        make.bottom.equalTo(self.scrollView.mas_bottom);
        make.right.equalTo(self.scrollView.mas_right);
    }];

}

- (void)setupImageView {
    self.imageView = [UIImageView new];
    [self.imageView sd_setImageWithURL:[NSURL URLWithString: self.model.imageUrl]];
    [self.contentView addSubview:self.imageView];

    [self.imageView  mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentView);
        make.width.equalTo(self.superview);
        make.left.equalTo(self.contentView);
        make.height.equalTo(@(self.superview.bounds.size.width / WIDTH_RATIO * HEIGHT_RATIO));
        make.right.equalTo(self.contentView);
    }];
}

- (void)setupScrollView {
    self.scrollView = [UIScrollView new];

    [self.superview addSubview: _scrollView];
    [self.scrollView  mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.superview.mas_top);
        make.left.equalTo(self.superview.mas_left);
        make.bottom.equalTo(self.superview.mas_bottom);
        make.right.equalTo(self.superview.mas_right);
    }];
}

@end
