//
//  ApiService.h
//  Netco
//
//  Created by Alexei Lazarev on 10/21/20.
//

#import <Foundation/Foundation.h>
#import "SportFeed.h"

NS_ASSUME_NONNULL_BEGIN

@interface ApiService : NSObject
typedef void (^SportFeedBlock)(NSArray<SportFeed *> *);

+ (void)requestData:(SportFeedBlock)callbackBlock;

@end

NS_ASSUME_NONNULL_END
