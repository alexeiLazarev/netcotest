//
//  SportFeed.m
//  Netco
//
//  Created by Alexei Lazarev on 10/21/20.
//

#import "SportFeed.h"

@implementation SportFeed

- (instancetype) initWithDictionary: (NSDictionary *)dict {
    self = [super init];

    if (self) {
        _body = dict[@"body"];
        _headline = dict[@"headline"];
        _shortHeadline = dict[@"shortHeadline"];
        _subHead = dict[@"subHead"];

        NSArray* taxonomy = dict[@"taxonomy"];
        NSDictionary* firstTaxonomy = taxonomy.firstObject;
        _name = firstTaxonomy[@"name"];

        NSDictionary* featuredMedia = dict[@"featuredMedia"];
        NSDictionary* context = featuredMedia[@"context"];
        _imageUrl = context[@"thumbnail_resized_800"];
    }
    return self;
}

@end
