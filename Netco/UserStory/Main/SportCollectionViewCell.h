//
//  SporCellCollectionViewCell.h
//  Netco
//
//  Created by Alexei Lazarev on 10/22/20.
//

#import <UIKit/UIKit.h>
#import "SDWebImage.h"

NS_ASSUME_NONNULL_BEGIN

@interface SportCollectionViewCell : UICollectionViewCell

@property (strong, nonatomic) UIImageView *imageView;
@property (strong, nonatomic) UILabel *titleLabel;
@property (strong, nonatomic) UILabel *textLabel;

@end

NS_ASSUME_NONNULL_END
