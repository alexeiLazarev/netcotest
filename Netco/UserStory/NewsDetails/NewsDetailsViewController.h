//
//  NewsDetailsViewController.h
//  Netco
//
//  Created by Alexei Lazarev on 10/22/20.
//

#import <UIKit/UIKit.h>
#import "SportFeed.h"

NS_ASSUME_NONNULL_BEGIN

@interface NewsDetailsViewController : UIViewController

@property (strong, nonatomic) SportFeed* model;

@end

NS_ASSUME_NONNULL_END
