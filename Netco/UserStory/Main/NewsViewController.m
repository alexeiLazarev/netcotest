//
//  ViewController.m
//  Netco
//
//  Created by Alexei Lazarev on 10/21/20.
//

#import "NewsViewController.h"
#import "ApiService.h"
#import "Masonry.h"
#import "SportCollectionViewCell.h"
#import <SDWebImage/SDWebImage.h>
#import "NewsDetailsViewController.h"

#define HEIGHT_RATIO 9
#define WIDTH_RATIO 16

@interface NewsViewController ()

@property (strong, nonatomic) NSArray<SportFeed *> * feedsArray;
@property (strong, nonatomic, nonnull) UICollectionView* collectionView;
@property (strong, nonatomic) UIActivityIndicatorView *activityIndicator;

@end

@implementation NewsViewController

// MARK: - View life cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = NSLocalizedString(@"NewsViewControllerTitle", nil);
    [self createUI];

    [self.activityIndicator startAnimating];

    __weak __typeof__(self) weakSelf = self;
    [ApiService requestData:^(NSArray<SportFeed *> * feedsArray) {
        weakSelf.feedsArray = feedsArray;
        dispatch_async(dispatch_get_main_queue(), ^{
            [weakSelf.activityIndicator stopAnimating];
            [weakSelf.collectionView reloadData];
        });
    }];
}

- (void)viewWillLayoutSubviews {
    [super viewWillLayoutSubviews];

    [self.collectionView.collectionViewLayout invalidateLayout];
}

// MARK: - UI methods

- (void)createUI {
    UIView *superview = self.view;

    UICollectionViewFlowLayout* flowLayout = [[UICollectionViewFlowLayout alloc] init];
    [flowLayout setScrollDirection:UICollectionViewScrollDirectionVertical];
    self.collectionView = [[UICollectionView alloc] initWithFrame:self.view.bounds collectionViewLayout:flowLayout];
    self.collectionView.backgroundColor = UIColor.whiteColor;
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;

    [self.collectionView registerClass:[SportCollectionViewCell class] forCellWithReuseIdentifier:@"cellIdentifier"];

    [superview addSubview: self.collectionView];

    UIEdgeInsets padding = UIEdgeInsetsMake(0, 0, 0, 0);

    [self.collectionView  mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(superview.mas_top).with.offset(padding.top);
        make.left.equalTo(superview.mas_left).with.offset(padding.left);
        make.bottom.equalTo(superview.mas_bottom).with.offset(padding.bottom);
        make.right.equalTo(superview.mas_right).with.offset(padding.right);
    }];

    self.activityIndicator = [UIActivityIndicatorView new];
    self.activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyleLarge;

    [superview addSubview: self.activityIndicator];

    [self.activityIndicator mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(superview.mas_centerX);
        make.centerY.equalTo(superview.mas_centerY);
    }];

}

// MARK: - Collection view data source

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.feedsArray.count;
}

- (nonnull __kindof UICollectionViewCell *)collectionView:(nonnull UICollectionView *)collectionView
                                   cellForItemAtIndexPath:(nonnull NSIndexPath *)indexPath {
    SportCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cellIdentifier" forIndexPath:indexPath];

    [cell.imageView sd_setImageWithURL:[NSURL URLWithString: _feedsArray[indexPath.row].imageUrl]];
    cell.titleLabel.text = self.feedsArray[indexPath.row].name;
    cell.textLabel.text = self.feedsArray[indexPath.row].shortHeadline.uppercaseString;

    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    int bottomSpaceValue = 60;
    int spaceBetween = 10;

    switch (UIDevice.currentDevice.userInterfaceIdiom) {
        case UIUserInterfaceIdiomPad:
            return CGSizeMake(self.view.frame.size.width / 2 - spaceBetween, self.view.frame.size.width / 2 / WIDTH_RATIO * HEIGHT_RATIO + bottomSpaceValue);
        default:
            return CGSizeMake(self.view.frame.size.width, self.view.frame.size.width / WIDTH_RATIO / 2 * HEIGHT_RATIO);
    }

    return CGSizeMake(self.view.frame.size.width, self.view.frame.size.width / WIDTH_RATIO / 2 * HEIGHT_RATIO);
}

// MARK: - Collection view delegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    NewsDetailsViewController* vc = [NewsDetailsViewController new];
    vc.model = self.feedsArray[indexPath.row];
    [self.navigationController pushViewController:vc animated:true];
}

@end
