//
//  SportFeed.h
//  Netco
//
//  Created by Alexei Lazarev on 10/21/20.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface SportFeed : NSObject

@property (strong, nonatomic) NSString *imageUrl;
@property (strong, nonatomic) NSString *body;
@property (strong, nonatomic) NSString *headline;
@property (strong, nonatomic) NSString *name;
@property (strong, nonatomic) NSString *shortHeadline;
@property (strong, nonatomic) NSString *subHead;

- (instancetype) initWithDictionary: (NSDictionary *)dict;

@end

NS_ASSUME_NONNULL_END
