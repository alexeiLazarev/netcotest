//
//  SporCellCollectionViewCell.m
//  Netco
//
//  Created by Alexei Lazarev on 10/22/20.
//

#import "SportCollectionViewCell.h"
#import "Masonry.h"

@implementation SportCollectionViewCell

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame: frame];

    if (self) {
        switch (UIDevice.currentDevice.userInterfaceIdiom) {
            case UIUserInterfaceIdiomPhone:
                [self setupViewsForIphone];
                break;
            case UIUserInterfaceIdiomPad:
                [self setupViewsForIpad];
                break;
            default:
                break;
        }
    }

    return self;
}

- (void)setupViewsForIphone {
    UIView* superview = self.contentView;

    UIEdgeInsets imageViewPadding = UIEdgeInsetsMake(0, 0, 0, 0);

    self.imageView = [UIImageView new];


    [superview addSubview: _imageView];

    [self.imageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(superview.mas_top).with.offset(imageViewPadding.top);
        make.left.equalTo(superview.mas_left).with.offset(imageViewPadding.left);
        make.bottom.equalTo(superview.mas_bottom).with.offset(-imageViewPadding.bottom);
        make.right.equalTo(superview.mas_centerX).with.offset(-imageViewPadding.right);
    }];

    self.titleLabel = [UILabel new];
    [superview addSubview: _titleLabel];
    [self.titleLabel setFont:[UIFont systemFontOfSize:14]];

    UIEdgeInsets titleLabelPadding = UIEdgeInsetsMake(8, 8, 0, 8);

    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(superview.mas_top).with.offset(titleLabelPadding.top);
        make.left.equalTo(_imageView.mas_right).with.offset(titleLabelPadding.left);
        make.right.equalTo(superview.mas_right).with.offset(-titleLabelPadding.right);
    }];

    self.textLabel = [UILabel new];
    self.textLabel.numberOfLines = 2;
    [superview addSubview: _textLabel];
    [self.textLabel setFont:[UIFont systemFontOfSize:13 weight:UIFontWeightSemibold]];

    UIEdgeInsets textLabelPadding = UIEdgeInsetsMake(8, 8, 0, 8);

    [self.textLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.titleLabel.mas_bottom).with.offset(textLabelPadding.top);
        make.left.equalTo(self.imageView.mas_right).with.offset(textLabelPadding.left);
        make.right.equalTo(superview.mas_right).with.offset(-textLabelPadding.right);
    }];
}

- (void)setupViewsForIpad {
    UIView* superview = self.contentView;

    UIEdgeInsets imageViewPadding = UIEdgeInsetsMake(0, 0, 60, 0);
    if (self.imageView == NULL) {
        self.imageView = [UIImageView new];
    }

    [superview addSubview: self.imageView];

    [self.imageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(superview.mas_top).with.offset(imageViewPadding.top);
        make.left.equalTo(superview.mas_left).with.offset(imageViewPadding.left);
        make.bottom.equalTo(superview.mas_bottom).with.offset(-imageViewPadding.bottom);
        make.right.equalTo(superview.mas_right).with.offset(-imageViewPadding.right);
    }];

    self.titleLabel = [UILabel new];
    [superview addSubview: self.titleLabel];
    [self.titleLabel setFont:[UIFont systemFontOfSize:13]];

    UIEdgeInsets titleLabelPadding = UIEdgeInsetsMake(8, 8, 0, 8);

    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_imageView.mas_bottom).with.offset(titleLabelPadding.top);
        make.left.equalTo(superview.mas_left).with.offset(titleLabelPadding.left);
        make.right.equalTo(superview.mas_right).with.offset(-titleLabelPadding.right);
    }];

    self.textLabel = [UILabel new];
    self.textLabel.numberOfLines = 2;
    [superview addSubview: self.textLabel];
    [self.textLabel setFont:[UIFont systemFontOfSize:12 weight:UIFontWeightSemibold]];

    UIEdgeInsets textLabelPadding = UIEdgeInsetsMake(8, 8, 0, 8);

    [self.textLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_titleLabel.mas_bottom).with.offset(textLabelPadding.top);
        make.left.equalTo(superview.mas_left).with.offset(textLabelPadding.left);
        make.right.equalTo(superview.mas_right).with.offset(-textLabelPadding.right);
    }];
}

@end
