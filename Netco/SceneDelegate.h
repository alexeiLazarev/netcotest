//
//  SceneDelegate.h
//  Netco
//
//  Created by Alexei Lazarev on 10/21/20.
//

#import <UIKit/UIKit.h>

@interface SceneDelegate : UIResponder <UIWindowSceneDelegate>

@property (strong, nonatomic) UIWindow * window;

@end

