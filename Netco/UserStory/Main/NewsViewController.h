//
//  ViewController.h
//  Netco
//
//  Created by Alexei Lazarev on 10/21/20.
//

#import <UIKit/UIKit.h>

@interface NewsViewController : UIViewController<UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>


@end

