//
//  ApiService.m
//  Netco
//
//  Created by Alexei Lazarev on 10/21/20.
//

#import "ApiService.h"
#import "SportFeed.h"

@implementation ApiService

+ (void)requestData:(SportFeedBlock)callbackBlock {
    NSMutableURLRequest *urlRequest = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:@"https://api.beinsports.com/contents?taxonomy[]=633&order[publishedAt]=desc&status=5&channel=1&site=4&itemsPerPage=50"]];

    [urlRequest setHTTPMethod:@"GET"];

    NSURLSession *session = [NSURLSession sharedSession];

    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:urlRequest completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
      NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
      if(httpResponse.statusCode == 200) {
        NSError *parseError = nil;
        NSDictionary *responseDictionary = [NSJSONSerialization JSONObjectWithData:data options:0 error:&parseError];
        NSArray *sportFeedsDictionary = responseDictionary[@"hydra:member"];

        NSMutableArray* feedsArray = [NSMutableArray new];

        for (NSDictionary* feed in sportFeedsDictionary) {
            [feedsArray addObject: [[SportFeed alloc] initWithDictionary:feed]];
        }

          callbackBlock(feedsArray);
      } else {
          callbackBlock([NSArray new]);
      }
    }];

    if (dataTask != nil) {
        [dataTask resume];
    } else {
        callbackBlock([NSArray new]);
    }
}


@end
